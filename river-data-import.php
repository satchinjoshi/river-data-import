<?php
/**
 * Plugin Name: River Data Import
 * Description: Pulls the data from the http://waterdata.usgs.gov/nwis and the http://www.dwr.state.co.us
 * Version: 0.8
 * Author: Sachin Joshi
 * License: GPL2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; /*Exit if accessed directly*/
}

define( 'CATCH_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

$time = ( get_option( '_river_cache_data_catch' ) ) ? ( get_option( '_river_cache_data_catch' ) * 60 * 60 ) : ( 12 * 60 * 60 );
/** Set the time for the cache, input the time in second */
define( 'CATCH_CACHE_THE_DATA_TIME', $time );

$files = array(
	'class-shortcode',
	'class-admin',
);
foreach ( $files as $file ) {
	require_once 'include/' . $file . '.php';
}
